package com.zds.register.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @创建人 zhj
 * @创建时间 2022/1/12
 * @描述
 */

@EnableEurekaServer
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})//DataSourceAutoConfiguration会自动加载.可以排除此类的自动配置，在启动类中加入

public class ApplicationEureka {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationEureka.class,args);
    }
}
