package com.zds.payment.service;



import org.apache.ibatis.annotations.Param;
import com.zds.commons.entities.Payment;
/**
 * @创建人 zhj
 * @创建时间 2022/1/9
 * @描述
 */
public interface PaymentService {
    public int create(Payment payment);

    public Payment getPaymentById(@Param("id") Long id);
}
