package com.zds.payment.service.impl;


import com.zds.commons.entities.Payment;
import com.zds.payment.dao.PaymentDao;

import com.zds.payment.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @创建人 zhj
 * @创建时间 2022/1/9
 * @描述
 */
@Service
public class PaymentServiceImpl implements PaymentService {
    @Resource
    private PaymentDao paymentDao;

    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}
