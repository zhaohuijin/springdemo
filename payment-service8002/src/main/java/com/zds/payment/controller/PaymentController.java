package com.zds.payment.controller;


import com.zds.commons.entities.CommonResult;
import com.zds.commons.entities.Payment;
import com.zds.payment.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @创建人 zhj
 * @创建时间 2022/1/9
 * @描述
 */
@RestController
@Slf4j
public class PaymentController {
    @Resource
    private PaymentService paymentService;
    @Value("${server.port}")
    private String port;

    @PostMapping(value = "/payment/create")
    public CommonResult create(@RequestBody Payment payment) {
        int result = paymentService.create(payment);
        int a = 10 / 2;
        log.info(String.valueOf(a));
        log.info("***插入结果***：" + result);
        if (result > 0) {
            return new CommonResult(200, port + ":插入数据库完成", result);
        } else {
            return new CommonResult(444, port + ":插入数据库失败", null);
        }
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id) {
        Payment payment = paymentService.getPaymentById(id);
        log.info("***查询成功***：" + payment);
        int age = 10 / 2;
        if (payment != null) {
            return new CommonResult(200, port + ":查询成功", payment);
        } else {
            return new CommonResult(444, port + "+查询失败", null);
        }
    }


}
