package com.zds.commons.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @创建人 zhj
 * @创建时间 2022/1/9
 * @描述
 */
@Data //生成getter,setter等函数
@NoArgsConstructor //生成无参构造函数
@AllArgsConstructor //生成全参数构造函数
public class Payment implements Serializable {
    private Long id;
    private String serial;
    private Double amount;

}
