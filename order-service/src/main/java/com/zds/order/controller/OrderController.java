package com.zds.order.controller;





import com.zds.commons.entities.CommonResult;
import com.zds.commons.entities.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

/**
 * @创建人 zhj
 * @创建时间 2022/1/9
 * @描述
 */
@RestController
@Slf4j
public class OrderController {
    public static final String PAYMENT_URL="http://PAYMENT-SERVICE";
    @Resource
    private RestTemplate restTemplate;

    @Resource
    private DiscoveryClient discoveryClient;

    @PostMapping(value = "/order/create")
    public CommonResult<Payment> create(@RequestBody Payment payment) {

        return restTemplate.postForObject(PAYMENT_URL+"/payment/create",payment, CommonResult.class);
    }

    @GetMapping(value = "/order/get/{id}")
    public CommonResult<Payment> getPayment(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PAYMENT_URL+"/payment/get/"+id, CommonResult.class);
    }
    @GetMapping(value = "/order/getPaymentServiceFromEureka")
    public Object discover(){

        List<String> services = discoveryClient.getServices();
        for(String element:services){
            log.info("*****services:"+element);
        }
        List<ServiceInstance> serviceInstances = discoveryClient.getInstances("PAYMENT-SERVICE");
        for (ServiceInstance instance:serviceInstances){
            log.info(instance.getServiceId()+"\t"+instance.getHost()+"\t"+instance.getPort()+"\t"+instance.getUri());
        }
        return this.discoveryClient;

    }
}
