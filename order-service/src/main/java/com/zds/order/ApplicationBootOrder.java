package com.zds.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @创建人 zhj
 * @创建时间 2022/1/9
 * @描述
 */
@SpringBootApplication
@EnableEurekaClient
public class ApplicationBootOrder {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationBootOrder.class, args);
    }
}
