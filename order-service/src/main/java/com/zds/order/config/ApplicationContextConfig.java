package com.zds.order.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @创建人 zhj
 * @创建时间 2022/1/11
 * @描述
 */
@Configuration   //@Configuration与@Bean 组成一对，作用是applicationContext.xml 将
//ApplicationContextConfig 注入到容器中去

public class ApplicationContextConfig {
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
